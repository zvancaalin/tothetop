#!shell/bash.exe
lastArg="${!#}"

rm -f -- "$lastArg"

length=$(($#-1))
array=${@:1:$length}

for arg in $array
do
    echo "/* ***** $arg ******* */" >> "$lastArg"
    cat $arg >> "$lastArg"
    echo "/* **************** */" >> "$lastArg"
done