 
        
        
        
        
        $("body").on("touchstart", "#chooseColor", function(){
            //alert(rgbToHex($(".colorDot.active").css("background-color")));
            //alert("merge");
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/set/_mainColor',
                data: {
                    'fingerprint': fingerprint,
                    'clothes_id':clothes_id,
                    'color':rgbToHex($(".dot.main").css("background-color"))
                },
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    //alert(dataR);
                        if(isJson(dataR)){
                            var json = JSON.parse(dataR);
                            if(typeof json.error!=="undefined"){
                                logThis("add", "mainColor", json.error, json, "Main Color not json");
                                return false;
                            }     
                        
                            if(typeof json.rez!=="undefined"){
                                if(json.rez=="success"){
                                    $("#loader").fadeOut(400);
                                    location.href="../index.html#1#success?You have added the product successfully";
                                }else{
                                    $("#loader").fadeOut(400);
                                    logThis("add", "mainColor", "rez", "", "rez != success");
                                    //alert("Something happened! Try again later!");
                                }
                            }else{
                                $("#loader").fadeOut(400);
                                logThis("add", "mainColor", "rez", "", "rez is missing");
                            }
                        }else{
                            logThis("add", "chooseColor", "", json, "Json response not properly formatted!");
                        }
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    $("#loader").fadeOut(400);
                    //alert(xhr.status);
                    logThis("add", "mainColor", xhr.status, thrownError, xhr.responseText);
                  }
            });
            
        });
        
        
        
        $("#product_name").on("keyup", function(){ // IF THERE IS TYPING IN PRODUCT NAME FIELD SHOW THE REST OF THE FORM
            if($("#typeNtags").hasClass("hide"))$("#typeNtags").removeClass("hide");
        });
        
        
        
        $("body").on("touchstart", ".colorDot", function(){ // VERY QUICK EVENT FOR MAIN COLOR CHOOSING
            $(".colorSpace").find(".active").removeClass("active");
            $(this).addClass("active");
        });
        
        
       /* $("body").on("touchend", "#backButton", function(){
            location.href="index.html";
        });*/



var touchmoved3; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('body').on('touchend', "#backButton", function(e){
            if(touchmoved3 != true){
                location.href="../index.html";
            }
        }).on('touchmove', function(e){
            touchmoved3 = true;
        }).on('touchstart', function(){
            touchmoved3 = false;
        });
         
        
        ///  ADD CLOTHES FORM SUBMIT
        
            
        $("body").on("touchend", "#submitAdd", function(e){ // ON SUBMIT TAP
            $(this).addClass("disabled");
            $(this).removeAttr("id");
            e.preventDefault();
            $(".loadme-mask").text("Uploading Image"); // SHOW SOME LOADING DETAILS
            $(".loadme-mask").addClass("loadtext");
            $("#loader").show();
            
            var fileURL = $("#imageRow").find("input").val(); // GET THE FILE INPUT
            var theFilename =""; // DEFAULT FILENAME
            if(fileURL!=""){
                theFilename = genId(6)+".jpg"; //IF IMAGE CHOSEN 
            }
            if(theFilename!="")uploadImage(theFilename);   //IF THERE IS AN IMAGE SELECTED UPLOAD THE IMAGE THAN SUBMIT THE FORM DUE TO ASYNCRON

            else submitTheForm(theFilename); //IF THERE IS NO IMAGE JUST SUBMIT THE FORM
            //alert(dataSend);


        });
        
        
        
        
        
        
        
        
        
        var touchmoved2; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#tagSpace').on('touchend', "label.btn", function(e){
            if(touchmoved2 != true){
                $(this).button("toggle");
            }
        }).on('touchmove', function(e){
            touchmoved2 = true;
        }).on('touchstart', function(){
            touchmoved2 = false;
        });
         


$('.dotsContainer').on("touchend", ".dot", function()
        {
            $('.dot.main').finish();
            if($(this).hasClass("main"))return false;
            var dot=$(this);
            var poz = dot.attr('class').split(' ')[1];
            $(this).animate({
                top: $('.dot.main').css('top'),
                left: $('.dot.main').css('left'),
                width: $('.dot.main').css('width'),
                height: $('.dot.main').css('height')
            }, 300);

            $('.dot.main').finish().animate({
                top: dot.css('top'),
                left: dot.css('left'),
                width: dot.css('width'),
                height: dot.css('height')
            }, 300);
            $('.dot.main').removeClass("main").addClass(poz);
            dot.removeClass(poz).addClass("main");
            
        });


        
       $("#theForm").on("submit", function(e){
            e.preventDefault();
       });
        
        
        // CAMERA BUTON EVENTS 
        
        $("body").on("touchstart", "#openCamera", function(){
           openCamera(); 
        });
        
        $("body").on("touchstart", "#openAlbum", function(){
          $("#loader").show();
           openAlbum(); 
        });
