function genId(wide) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 1; i <= wide; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}

function isJson(str) {
    try {
        JSON.parse(str);
    }catch(err){
        return false;
    }
    return true;
}

function componentFromStr(numStr, percent) {
    var num = Math.max(0, parseInt(numStr, 10));
    return percent ?
        Math.floor(255 * Math.min(100, num) / 100) : Math.min(255, num);
}

function rgbToHex(rgb) {
    var rgbRegex = /^rgb\(\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*\)$/;
    var result, r, g, b, hex = "";
    if ( (result = rgbRegex.exec(rgb)) ) {
        r = componentFromStr(result[1], result[2]);
        g = componentFromStr(result[3], result[4]);
        b = componentFromStr(result[5], result[6]); 
        hex = "#" + (0x1000000 + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }
    return hex;
}


function logThis(page, action, status, error, text){
    alert("Something happened! Try again later!");
    var dataSend = {
        "page": page,
        "action": action,
        "status": status,
        "error": error,
        "text": text
    };
   // alert(action + " / "+status+" / "+error+" / "+text);
    $.ajax({
        url: 'http://'+base+"."+url+'/apicall/safe/_logThis',
        type: 'POST',
        data: dataSend,
        success: function(dataR) {
           // alert(dataR);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert("status: "+xhr.status);
            //alert(thrownError);
          }
    });

    if(status=="410")location.reload();
    $("#loader").fadeOut(400);
}

function freeze(showw){
showw = (typeof showw !== 'undefined') ?  showw : true;
    if(showw == true){
        $("#freeze").removeClass("hide");
    }else{
        $("#freeze").addClass("hide");
    }
}



function notifyThis(type, text, hider){
hider = (typeof hider !== 'undefined') ?  hider : true;
     var notifier = $("#notifier");
                
    notifier.find("span#alertText").html(text+"!"); // FILL THE CONTENT

    notifier.addClass("alert-"+type); // SET THE NOTIFICATION TYPE

    notifier.removeClass("hide"); // SHOW THE NOTIFICATION
    if(hider)
    notifier.delay(3000).slideUp(500, function(){ // HIDE THE NOTIFICATION
        notifier.slideUp(500, function(){
            notifier.addClass("hide");
        });
    }); 
}
