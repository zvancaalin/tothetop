        $("body").on("touchend", "#addnew", function(){ // VERY QUICK EVENT TRIGGER FOR ADD PAGE REDIRECT
            location.href="add_page/add.html";
        });
        
        
        
        $("body").on("touchend", "#closeWardrobeItem", function(e){ // VERY QUICK EVENT TRIGGER FOR MODAL CLOSE
            e.preventDefault();
            $(".modal").modal("hide");
            e.stopPropagation();
        });
        
        
        
        $('body').on("touchend", "#tablist a", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
           // $(this).tab("show");
            location.href=$(this).attr("href"); 
        });
        
        $('body').on("touchend", "#wtablist a", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
            $(this).tab("show");
            //location.href=$(this).attr("href"); 
        });
        
        $('body').on("touchend", "#showRestColorsTemplate", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
            //alert($("#colorSpace.rest").html());
            if($("#colorSpace.rest").is(":hidden")){
                $(".headerScrollable").height($("#topPanelContent").height());
            }
            $("#colorSpace.rest").slideToggle(200, function(){
                if($("#colorSpace.rest").is(":hidden")){
                    $("#showRestColorsTemplate").find("span").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down").css("top", "2px");
                }else{
                    $("#showRestColorsTemplate").find("span").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up").css("top", "0px");
                }
            });
        });
        
        
        
        
        var touchmoved3; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#tagSpace').on('touchend', "label.btn", function(e){
            if(touchmoved3 != true){
                e.preventDefault();
                $(this).button("toggle");
                //e.stopPropagation(); 
                
                
                //alert($(this).wrap('<p/>').parent().html());
            }
        }).on('touchmove', function(e){
            touchmoved3 = true;
        }).on('touchstart', function(){
            touchmoved3 = false;
        });
        
        
         var touchmoved4; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#colorSpace, #colorSpace.rest').on('touchend', "label.colorSelect", function(e){
            if(touchmoved4 != true){
                e.preventDefault();
                $(this).button("toggle");
                //e.stopPropagation(); 
            }
        }).on('touchmove', function(e){
            touchmoved4 = true;
        }).on('touchstart', function(){
            touchmoved4 = false;
        });
        
        
        
        var touchmoved5; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('header.header').on('touchend', "#hidePanel", function(e){
            if(touchmoved5 != true){
                $(".headerScrollable").slideToggle(300, function(){
                    if($(".headerScrollable").is(":hidden"))
                        $("#hidePanel").text("Show");
                    else
                        $("#hidePanel").text("Hide");
                });
            }
        }).on('touchmove', function(e){
            touchmoved5 = true;
        }).on('touchstart', function(){
            touchmoved5 = false;
        });

var touchmoved7; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('.onboarding').on('touchend', ".skipButton", function(e){
            if(touchmoved7 != true){
                $(".onboarding").remove();
            }
        }).on('touchmove', function(e){
            touchmoved7 = true;
        }).on('touchstart', function(){
            touchmoved7 = false;
        });
        
        
        
        $("header").on("touchend", "#showMe", function(e){
            e.preventDefault();
            
            $("#loader").show();  
            
            generateInput($("#topPanelContent"));
            
            $("#umerasCustom").removeClass("hide");//SHOW THE LOADING DOTS
            
            dataSend={ // SERIALIZE THE FORM AND CREATE THE DATA ARRAY
                  "form":  $("#topPanelContent").serializeArray(),
                  "fingerprint": fingerprint
            };
            //alert(fingerprint);
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_customRandomizer',
                data: dataSend,
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST', 
                success: function(dataR) { 
                   //alert(dataR);
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            $("#loader").fadeOut(400); 
                            alert(json.error); 
                            return false;
                        }
                    }  
                       
                    //alert(dataR);
                    $("#umerasCustom").html(dataR);
                    
                    $("#umerasCustom .dresspot").css("animation-name", "none"); 
                    
                    $(".headerScrollable").slideUp(300, function(){
                            $("#hidePanel").text("Show");
                    });
                    
                    $("#loader").fadeOut(200); // HIDE THE LOADER
                    //$("#randomize").removeAttr("disabled"); 
                     
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert("status: "+xhr.responseText);
                    //alert(thrownError); 
                    $("#loader").fadeOut(400); 
                    logThis("index", "customRandomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
        });  
         
        //// RANDOMIZE CLICK
        $("body").on("touchend", "#randomize", function(){
            //$(this).addClass("disabled");
            $("#loader").show();
            $(this).css("color", "#fff");
            $('.carousel').flickity( 'select', 0 );
            
            $("#splash").addClass("hide");//HIDE THE LOGO
            
            $("#umeras").removeClass("hide");//SHOW THE LOADING DOTS
            
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/get/_Randomizer',
                data: {
                    'fingerprint':fingerprint,
                },
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400);  
                            return false;
                        }
                    } 
                    //alert(dataR);
                    $("#umeras").html(dataR);
                    $(".dresspot").css("animation-name", "none"); 
                    $("#loader").fadeOut(200); // HIDE THE LOADER
                   // $("#randomize").removeAttr("disabled");
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
       
        });

        

    ////// DELETE EVENT
 $("body").on("touchend", ".deleteClothes", function(){
            //$(this).addClass("disabled");
            $("#loader").show();
            
            var clothes_id = $(this).attr("data-id");
            
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/set/_deleteClothes',
                data: {
                    'fingerprint':fingerprint,
                    'clothes_id':clothes_id
                },
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400); 
                            return false;
                        } 
                        if(typeof json.rez!=="undefined"){
                            if(json.rez=="success"){
                                
                                $("#loader").fadeOut(400);
                                location.href="index.html#wardrobe#success?You have deleted the product successfully";
                                location.reload();
                            }else{
                                $("#loader").fadeOut(400);
                                logThis("add", "deleteClothes", "rez", "", "rez != success");
                                //alert("Something happened! Try again later!");
                            }
                        }else{
                            $("#loader").fadeOut(400);
                            logThis("add", "deleteClothes", "rez", "", "rez is missing");
                        }
                    }else{
                        logThis("add", "deleteClothes", "", "", "Json response not properly formatted!");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
       
        });
        
        
        
        ///////// MODAL CONTENT GETTERS
        




var touchmoved; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#dulap').on('touchend', " .wardrobeChild", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            //alert("merge");
            if(touchmoved != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!== "undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //alert("status: "+xhr.status);
                            //alert(thrownError);
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved = false; // IT MEANS IT DOESN'T MOVING YET
        });
       
        
        var touchmoved2; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#umeras').on('touchend', ".dresspot", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            //alert("merge");
            if(touchmoved2 != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!=="undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent2").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal2').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent:home", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved2 = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved2 = false; // IT MEANS IT DOESN'T MOVING YET
        });
        



var touchmoved6; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#etc').on('touchend', ".dresspot", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            
            if(touchmoved6 != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!=="undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent3").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal3').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent:home", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved6 = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved6 = false; // IT MEANS IT DOESN'T MOVING YET
        });
        
        
        
        
        ///////// END OF MODAL CONTENT GETTERS
        







var gnStartTime = 0;
var gbMove = false;
var gbStillTouching = false;
var show = 0;
function checkTapHold(nID) {
  if ((!gbMove) && (gbStillTouching) && (gnStartTime == nID)) {
    gnStartTime = 0;
    gbMove = false; 
    //alert('tap hold event'); 
      if(!show){
          show=1;
          $(".modal-body").css("background", "rgba(0,0,0,0.2)");
      }
  }
}

window.addEventListener('touchstart',function(event) {
  gbMove = false;
  gbStillTouching = true;
  gnStartTime = Number(new Date());
  setTimeout('checkTapHold(' + gnStartTime + ');',300);
},false);

window.addEventListener('touchmove',function(event) {
  gbMove = true;
},false);

window.addEventListener('touchend',function(event) {
  gbStillTouching = false;
    $(".modal-body").css("background", "rgba(0,0,0,0.7)");
    show=0;
},false);

