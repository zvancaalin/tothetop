Every App Page Structure:

Folder name: <page_name>_page =>

<page_name>_css Folder
<page_name>_js Folder
custom_<page_name>_css Folder
custom_<page_name>_js Folder
<page_name>.html Main File
<page_name>_app_source.css Generated Style File
<page_name>_app_source.js Generated Javascript File

Create the app file sources:

Index Javscript: bash createSource.sh index_js/*.js custom_index_js/*.js index_app_source.js

Index CSS: bash createSource.sh index_css/*.css custom_index_css/*.css index_app_source.css

<page_name> Javascript: bash createSource.sh <page_name>_page/<page_name>_js/*.js <page_name>_page/custom_<page_name>_js/*.js <page_name>_page/<page_name>_app_source.js

<page_name> Javascript: bash createSource.sh <page_name>_page/<page_name>_css/*.css <page_name>_page/custom_<page_name>_css/*.css <page_name>_page/<page_name>_app_source.css