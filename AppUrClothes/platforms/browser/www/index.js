/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

var deviceStart = function(StatusBar){


            // MAIN TABS NAVIGATION TOUCH SWIPE

            var carousel = $('.carousel').flickity({
                prevNextButtons: false,
                pageDots: false,
                cellAlign: 'left',
                setGallerySize: false,
                selectedAttraction: 0.2,
                friction: 0.8,
                dragThreshold: 70,
                touchVerticalScroll: false
            });

            //alert("devicestart");
            $(".flickity-viewport, #clotheSpace").css("height", $(window).height()-110);

            var flkty = carousel.data('flickity');

            var cellButtonGroup = $('ul#tablist');
            var cellButtons = cellButtonGroup.find('li');

            // update selected cellButtons
            carousel.on( 'select.flickity', function() {
              cellButtons.filter('.active')
                .removeClass('active');
              cellButtons.eq( flkty.selectedIndex )
                .addClass('active');
            });

            // select cell on button click
            cellButtonGroup.on( 'touchend', 'li', function() {
              var index = $(this).index();
              carousel.flickity( 'select', index );
            });

             // MAIN TABS NAVIGATION TOUCH SWIPE




            if($(".onboarding").length>0){
                $('.onboarding').flickity({
                    prevNextButtons: false,
                    wrapAround: true,
                    dragThreshold: 50,
                    cellSelector: '.prez',
                    touchVerticalScroll: false
                });
                $('.onboarding').css("visibility", "visible");
            }







            urlManagement(); // MANAGE THE URL FOR TAB REDIRECTS
            notifierManagement(); // MANAGE THE NOTIFICATION


            console.log("fg2: "+ fingerprint);
             // REMOVE THE LOADER

            var data ={'fingerprint':fingerprint};

            $.post('http://'+base+"."+url+'/apicall/token/_token', data, function(dataR){
                //alert(dataR);
                var json = JSON.parse(dataR);
                store.setJWT(json.tm, json.fg);

                var storage = window.localStorage;
                var onBoarding = storage.getItem("onboarding"); // Pass a key name to get its value.
                //alert(onBoarding);
                if(onBoarding==null || onBoarding==0)setOnBoarding();
                //storage.removeItem("onboarding");
                //onBoardingPass();
                prepareWardrobe(); /// PREPARE THE WARDROBE
                generateData(); /// GENERATE THE DATE FOR SEARCHING
                getNoClothes(); /// GET THE NUMBER OF CLOTHES

                $("#loader").delay(400).fadeOut(400);

                $("#splashScreen").delay(1000).fadeOut(1000);
                StatusBar.hide();

            }).fail(function(xhr, ajaxOptions, thrownError){
                logThis("index", "customRandomizer", xhr.status, thrownError, xhr.responseText);
            });

        };


var offline=false;
var app = {
    test: function(){
        alert("text");
    },
    // Application Constructor
    initialize: function() {
          // alert("bind");
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        //alert("bind");
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('deviceready', this.checkConnection, false);
        document.addEventListener("resume", this.onResume, false);
        document.addEventListener("offline", this.toOffline, false);
        document.addEventListener("online", this.toOnline, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //console.log('Received Device Ready Event');
        //console.log('calling setup push');
        //app.setupPush();
        //navigator.splashscreen.show();
        //alert("Start");
        try{


            window.localStorage.setItem("uuid", device.uuid);
            setFingerprint(device.uuid);

            if(window.localStorage.getItem("onboarding")!=null)$(".onboarding").remove();
            deviceStart(StatusBar);
            StatusBar.hide();
        }catch(err){
            logThis("indexJs", "onDeviceReady", "none", err.message, "none");
            //alert("error index:"+err.message);
        }

    },
    onResume: function(){
        StatusBar.hide();
        this.checkConnection;
    },
    toOnline: function(){
        if(offline){
            location.href="index.html#home#success?Welcome Back!";
            location.reload();
        }
    },
    toOffline: function(){
        offline=true;
        freeze();
        notifyThis("warning", "Please connect to the internet!", false);
    },
    checkConnection: function(){
        var networkState = navigator.connection.type;

        var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = 'Cell 2G connection';
        states[Connection.CELL_3G]  = 'Cell 3G connection';
        states[Connection.CELL_4G]  = 'Cell 4G connection';
        states[Connection.CELL]     = 'Cell generic connection';
        states[Connection.NONE]     = 'No network connection';
        if(networkState==Connection.NONE){
            //alert('Connect to internet and try to open Dresoo again!');
           // navigator.app.exitApp();
            this.toOffline;
        }
    }
};

