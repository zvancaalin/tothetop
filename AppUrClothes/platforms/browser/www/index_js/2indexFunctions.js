
        function setOnBoarding(){
            var data ={'fingerprint':fingerprint};
            //alert("called");
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/set/_onBoarding',
                data: {
                    'fingerprint':fingerprint,
                }, 
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function() {
                    var storage = window.localStorage;
                    storage.setItem("onboarding", 1); // Pass a key name and its value to add or update that key. 
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "setOnBoarding", xhr.status, thrownError, xhr.responseText);
                  }
            });
        }




        function prepareWardrobe(){ //PREPARING THE WARDROBE
            var mainRow = $(".wardroberow"); 
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:1},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    //alert(dataR);
                    var json = JSON.parse(dataR);
                    // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#head.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#head.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:2},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#top.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#top.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:3},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW

                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#bottom.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#bottom.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:4},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#shoes.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#shoes.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                   // mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:5},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#accesories.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#accesories.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                   // mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            mainRow.remove();
        }
           

        
        // GENERATE FORM INPUTS
        
        function generateInput(formWrap){
            formWrap.find("input").remove();
            $("#tagSpace .bigSelect.active").each(function(){
                var inputHTML="<input type='checkbox' name='tags[]' style='display:none;' value='"+$(this).attr("data-id")+"' checked>";
                formWrap.append(inputHTML);
                
               // alert($("#topPanelContent").html());
            });
            $("#colorSpace .colorSelect.active").each(function(){
                var inputHTML="<input type='checkbox' name='colors[]' style='display:none;' value='"+$(this).attr("data-id")+"' checked>";
                
                formWrap.append(inputHTML);
                
               // alert($("#topPanelContent").html());
            });
        }
        
        
        
        
        
        
        
        
        // GENERATE FORM DATA
        
        
        function generateData(){
            try{
            
                /// CREATE TAGS BUTTONS FROM DB


                var tagTemplate = $("#tagTemplate");


                $.ajax({
                    url: 'http://'+base+"."+url+'/apicall/safe/_tagsList', 
                    type: 'POST',
                    success: function(dataR) {

                        var json = JSON.parse(dataR);

                        for(var i=0; i<json.length; i++){

                            var currentTemplate = tagTemplate.clone();

                            if(i==0) {   // Check the first checkbox
                                currentTemplate.addClass("active");
                            }

                            currentTemplate.attr("data-id", json[i].id);   // Set The Value for each checkbox
                            currentTemplate.removeAttr("id");    // Remove ID attribute

                             // Get the input html ONLY

                            currentTemplate.html(json[i].name); // Change the Text for each checkbox
                            //alert(currentTemplate.html());
                            currentTemplate.appendTo("#tagSpace"); // Move the checkbox in right place
                            currentTemplate.removeClass("hide"); // Show the Checkbox



                        } 
                        tagTemplate.remove();


                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //alert("status: "+xhr.status);
                        //alert(thrownError);
                        logThis("index", "generateData:Tags", xhr.status, thrownError, xhr.responseText);
                      }
                });
                
                
                /// CREATE COLORS BUTTONS FROM DB


                var colorTemplate = $("#colorTemplate");
                var toggle = $("#showRestColorsTemplate");

                $.ajax({
                    url: 'http://'+base+"."+url+'/apicall/safe/_colorsList', 
                    type: 'POST',
                    success: function(dataR) {

                        var json = JSON.parse(dataR);

                        for(var i=0; i<json.length; i++){

                            var currentTemplate = colorTemplate.clone();

                            if(i==0) {   // Check the first checkbox
                                currentTemplate.addClass("active");
                            }
                            
                            currentTemplate.attr("data-id", json[i].id); // SET THE ID

                            currentTemplate.removeAttr("id");    // Remove ID attribute

                            currentTemplate.find(".colorDot").css("background-color", json[i].color);
                            //alert(currentTemplate.html());
                            if(i<3){
                                currentTemplate.appendTo("#colorSpace"); // Move the checkbox   in right place
                               
                            }else
                                currentTemplate.appendTo("#colorSpace.rest");    
                            
                            currentTemplate.removeClass("hide"); // Show the ColorDot
 


                        } 
                        colorTemplate.remove();
                        toggle.clone().removeClass("hide").appendTo("#colorSpace");
                        toggle.remove();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        logThis("index", "generateData:Colors", xhr.status, thrownError, xhr.responseText);
                      }
                });
            
            
            
            }catch(err){
                logThis("index", "generateData:ALL", "", err, "");
            }
            
       }
        
        
       


        
        function urlManagement(){ // URL MANAGEMENT FUNCTION
            
            var pathArray = location.href.split( '#' ); //GET THE URL AND SPLIT
            
            var secondLevelLocation = pathArray[1]; // GET THE STRING AFTER THE # SIGN
            
            var cellButtonGroup = $('ul#tablist');
            var cellButtons = cellButtonGroup.find('li');
            //alert(secondLevelLocation);
            if(typeof secondLevelLocation!=="undefined"){ // IF THERE IS ANY LINK SHOW THE TAB
                //alert(secondLevelLocation);
                cellButtons.filter('.active').removeClass('active');
                $('#tablist li').eq(secondLevelLocation).addClass("active");
                $('.carousel').flickity( 'select', secondLevelLocation); 
            }
            
        }
         
        function notifierManagement(){
            var pathArray = location.href.split( '#' ); //GET THE URL AND SPLIT
            
            var thirdLevelLocation = pathArray[2]; // GET THE STRING AFTER THE # SIGN
            //alert(secondLevelLocation);
            
            if(typeof thirdLevelLocation!=="undefined"){ // IF THERE IS ANY NOTIFICATION
                var splitAgain = thirdLevelLocation.split("?"); // SPLIT THE TYPE AND THE TEXT
                
                var type = splitAgain[0];
                
                var text = splitAgain[1];
                
                var notifier = $("#notifier");
                
                notifier.find("span#alertText").html(text+"!"); // FILL THE CONTENT
                
                notifier.addClass("alert-"+type); // SET THE NOTIFICATION TYPE
                
                notifier.removeClass("hide"); // SHOW THE NOTIFICATION
                
                notifier.delay(3000).slideUp(500, function(){ // HIDE THE NOTIFICATION
                    notifier.slideUp(500, function(){
                        notifier.addClass("hide");
                    });
                });   
            }
        }
        
        

        function getNoClothes(){
            var data ={'fingerprint':fingerprint};

            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/get/_noOfClothes',
                data: {
                    'fingerprint':fingerprint,
                }, 
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400); 
                            return false;
                        }
                        noClothes = json.nr;
                        if(noClothes>0 && json.a){
                            $("#home_title").addClass("hide");
                            $("#randomize").removeClass("disabled");
                        }else $("#randomize").removeAttr("id");
                        $("#loader").fadeOut(400);
                    } 
                    //alert(dataR);
                    
                     
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
        }
        
