
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

 function deviceStart(){

             var data ={'fingerprint':fingerprint}; // PREPARE THE FINGERPRINT FOR TOKEN GENERATE
             //var sendData=JSON.stringify(data);
             //alert('http://'+base+"."+url+'/apicall/token/_token');
             try{
                 $.post('http://'+base+"."+url+'/apicall/token/_token', data, function(dataR){
                     //alert(dataR);
                     var json = JSON.parse(dataR);
                     store.setJWT(json.tm, json.fg); // STORE THE TOKEN

                     //alert(json.fg);


                     //$("#loader").delay(1000).fadeOut(400);

                     generateData(); // GENERATE THE TYPES AND TAGS
                     checkGender(); // CHECK IF THE USER HAS SETTED THE GENDER
                     $("#loader").delay(200).fadeOut(400);

                 }).fail(function(error){
                     logThis("add", "deviceStart()", "", error, "Token Extract error");
                 });
             }catch(err){
                 logThis("add", "deviceStart()", "", error, "Token Extract Ajax Error");
             }
    $("body").on("touchend", function(){
        Keyboard.hide();
    });
}
var offline=false;
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener('deviceready', this.checkConnection, false);
        document.addEventListener("offline", this.toOffline, false);
        document.addEventListener("online", this.toOnline, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        //console.log('Received Device Ready Event');
        //console.log('calling setup push');
        //app.setupPush();
        //navigator.splashscreen.show();

        StatusBar.hide();
        window.localStorage.setItem("uuid", device.uuid);
        setFingerprint(device.uuid);
        console.log("Start");
        if(window.localStorage.getItem("onboarding")!=null)$(".onboarding").remove();
        deviceStart();
        
    },
    toOnline: function(){
        if(offline){
            location.href="index.html#home#success?Welcome Back!";
            location.reload();
        }
    },
    toOffline: function(){
        offline=true;
        freeze();
        notifyThis("warning", "Please connect to the internet!", false);
    },
    checkConnection:function(){
        var networkState = navigator.connection.type;

        var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI]     = 'WiFi connection';
        states[Connection.CELL_2G]  = 'Cell 2G connection';
        states[Connection.CELL_3G]  = 'Cell 3G connection';
        states[Connection.CELL_4G]  = 'Cell 4G connection';
        states[Connection.CELL]     = 'Cell generic connection';
        states[Connection.NONE]     = 'No network connection';
        if(networkState==Connection.NONE){
            //alert('Connect to internet and try to open Dresoo again!');
           // navigator.app.exitApp();
            this.toOffline;
        }
    },
    setupPush: function() {
        console.log('calling push init');
        var push = PushNotification.init({
            "android": {
                "senderID": "XXXXXXXX"
            },
            "browser": {},
            "ios": {
                "sound": true,
                "vibration": true,
                "badge": true
            },
            "windows": {}
        });
        console.log('after init');
        
        push.on('registration', function(data) {
            console.log('registration event: ' + data.registrationId);

            var oldRegId = localStorage.getItem('registrationId');
            if (oldRegId !== data.registrationId) {
                // Save new registration ID
                localStorage.setItem('registrationId', data.registrationId);
                // Post registrationId to your app server as the value has changed
            }

            var parentElement = document.getElementById('registration');
            var listeningElement = parentElement.querySelector('.waiting');
            var receivedElement = parentElement.querySelector('.received');

            listeningElement.setAttribute('style', 'display:none;');
            receivedElement.setAttribute('style', 'display:block;');
        });

        push.on('error', function(e) {
            console.log("push error = " + e.message);
        });

        push.on('notification', function(data) {
            console.log('notification event');
            navigator.notification.alert(
                data.message,         // message
                null,                 // callback
                data.title,           // title
                'Ok'                  // buttonName
            );
       });
    }
};
