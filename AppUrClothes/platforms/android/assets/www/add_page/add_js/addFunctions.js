
 




// CAMERA FUNCTIONS
        
        function createNewFileEntry(imgUri) {
            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {

                // JPEG file
                dirEntry.getFile("tempFile.jpeg", { create: true, exclusive: false }, function (fileEntry) {

                    // Do something with it, like write to it, upload it, etc.
                    // writeFile(fileEntry, imgUri);
                    console.log("got file: " + fileEntry.fullPath);
                    // displayFileData(fileEntry.fullPath, "File copied to");

                }, onErrorCreateFile);

            }, onErrorResolveUrl);
        }
        function setOptions(srcType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 20,
                destinationType: Camera.DestinationType.FILE_URI,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: false,
                correctOrientation: true  //Corrects Android orientation quirks
            }
            return options;
        }

function openCamera() {     

            var srcType = Camera.PictureSourceType.CAMERA;
            var options = setOptions(srcType);


            navigator.camera.getPicture(function cameraSuccess(imageUri) {
              $("#cameraHolder").remove();  // EMPTY THE CAMERA HOLDER
              $("#cameraIcons").css("background", "url("+imageUri+") 100% center/cover"); // PLACE THE CHOSEN IMAGE AS BACKGROUND
              $("#imageRow").find("input").val(imageUri); // SET THE VALUE OF THE INPUT FOR THE FORM
              $("#cameraIcons").removeClass("hide");// SHOW THE OVERLAY ICONS
              $("#nameNgender").removeClass("hide");
                
            }, function cameraError(error) {
                logThis("add", "openCamera()", "", error, "Unable to obtain picture");
                //alert("Unable to obtain picture: " + error, "app");
                
            }, options);
        }
        
        
        function openAlbum() {
            
            var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
            var options = setOptions(srcType);


            navigator.camera.getPicture(function cameraSuccess(imageUri) {
              $("#cameraHolder").remove();
              $("#cameraIcons").css("background", "url("+imageUri+") 100% center/cover");
                $("#imageRow").find("input").val(imageUri);
              $("#cameraIcons").removeClass("hide");
                $("#nameNgender").removeClass("hide");
                $("#loader").fadeOut(400); 
                
            }, function cameraError(error) {
                $("#loader").fadeOut(400); 
                //alert("Unable to obtain picture: " + error, "app");
                logThis("add", "openAlbum()", "", error, "Unable to obtain picture");
            }, options);
        }



        
        // GENERATE FORM DATA
        
        
        function generateData(){
            try{
            /// CREATE TYPE BUTTONS FROM DB
            
            
            var typeTemplate = $("#typeTemplate");
            
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/safe/_typesList', 
                type: 'POST',
                success: function(dataR) {
                    
                    var json = JSON.parse(dataR);
                    
                    for(var i=0; i<json.length; i++){
                        
                        var currentTemplate = typeTemplate.clone();
                        
                        if(i==0) {   // Check the first checkbox
                            currentTemplate.find("input").attr("checked", "checked");
                            currentTemplate.addClass("active");
                        }
                        
                        currentTemplate.find("input").attr("value", json[i].id);   // Set The Value for each checkbox
                        var htmlSoFar = currentTemplate.find("input").wrap('<p/>').parent().html();
                        currentTemplate.find("input").unwrap();
                        
                        currentTemplate.html(htmlSoFar + json[i].name); // Change the Text for each checkbox
                        //alert(currentTemplate.html());
                        currentTemplate.appendTo("#typeSpace"); // Move the checkbox in right place
                        currentTemplate.removeClass("hide"); // Show the Checkbox
                        
                        
                        
                    } 
                    typeTemplate.remove();
                    
                   /* $("#typeSpace").on("click", "label.btn", function(){
                            //alert($(this).parent().html());
                            $(this).button("toggle");
                        });*/
                    
                    var touchmoved1;
                    $('#typeSpace').on('touchend', "label.btn", function(e){
                        if(touchmoved1 != true){
                            $(this).button("toggle");
                        }
                    }).on('touchmove', function(e){
                        touchmoved1 = true;
                    }).on('touchstart', function(){
                        touchmoved1 = false;
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("add", "generateData:types", xhr.status, thrownError, xhr.responseText);
                  }
            });
            
            
            /// CREATE TAGS BUTTONS FROM DB
            
            
            var tagTemplate = $("#tagTemplate");
            
            
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/safe/_tagsList', 
                type: 'POST',
                success: function(dataR) {
                    
                    var json = JSON.parse(dataR);
                    
                    for(var i=0; i<json.length; i++){
                        
                        var currentTemplate = tagTemplate.clone();
                        
                        if(i==0) {   // Check the first checkbox
                            currentTemplate.find("input").attr("checked", "checked");
                            currentTemplate.addClass("active");
                        }
                        
                        currentTemplate.find("input").attr("value", json[i].id);   // Set The Value for each checkbox
                        currentTemplate.removeAttr("id");    // Remove ID attribute
                        
                        var htmlSoFar = currentTemplate.find("input").wrap('<p/>').parent().html();
                        currentTemplate.find("input").unwrap(); // Get the input html ONLY
                        
                        currentTemplate.html(htmlSoFar + json[i].name); // Change the Text for each checkbox
                        //alert(currentTemplate.html());
                        currentTemplate.appendTo("#tagSpace"); // Move the checkbox in right place
                        currentTemplate.removeClass("hide"); // Show the Checkbox
                        
                        
                        
                    } 
                    tagTemplate.remove();
                    

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("add", "generateData:tags", xhr.status, thrownError, xhr.responseText);
                  }
            });
            
            
            
            }catch(err){
                console.log(err);
            }
            
       }


////// SUBMIT THE FORM FUNCTION
        
        
        function submitTheForm(theFilename){
            dataSend={ // SERIALIZE THE FORM AND CREATE THE DATA ARRAY
                  "form":  $("#theForm").serializeArray(),
                  "image": theFilename,
                  "fingerprint": fingerprint
            };
            $(".loadme-mask").text("Processing Image Colors");
            $.ajax({
                    url: 'http://'+base+"."+url+'/apicall/set/_insertClothes',
                    data: dataSend,
                    beforeSend: function(request){
                        request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                        request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        
                    },
                    type: 'POST',
                    success: function(dataR) {
                        //alert(dataR);
                        if(isJson(dataR)){ // CHECK IF THE DATA RECEIVED IS IN JSON FORMAT
                            var json = JSON.parse(dataR);
                            if(typeof json.error!=="undefined"){ // CHECK FOR ERROR FIRST
                                //alert(json.error);
                                logThis("add", "submitTheForm()", json.error, json, "Submit the form not json");
                                $("#loader").fadeOut(400);
                                return false;
                            }    
                        
                            if(typeof json.rez!="undefined"){ // CHECK IF THERE IS ANY RESPONSE
                                if(json.rez=="success"){
                                    $("#loader").fadeOut(400);
                                    location.href="../index.html#1#success?You have added the product successfully";
                                }else{
                                    logThis("add", "submitTheForm()", json.rez, json, "Submit form rez not success");
                                }
                            }else{
                                clothes_id=json.clothes_id;
                                for(var i=0;i<5;i++){
                                    $(".dot:nth-child("+(i+1)+")").css("background-color", json[i][1]);
                                }
                                $("#loader").fadeOut(400);
                                $("#myModal").modal("show");
                            }
                        }else{
                            logThis("add", "submitTheForm()", json.error, json, "Json response not properly formatted!");
                        }
                        // Decode and show the returned data nicely.
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        $("#loader").fadeOut(400);
                        logThis("add", "submitTheForm()", xhr.status, thrownError, xhr.responseText);
                        ///// DELETE THE LAST ID
                      },
                    timeout: 8000
                });
        }
        
        
        
        function getColors(theFilename){
            var colorThief = new ColorThief();
            var source = $("#myHiddenImage");
            var colors = colorThief.getPalette(source, 5);
            console.log(colors);
            
        }
        
        
        
        
        /// UPLOAD THE IMAGE function
        
              
        function uploadImage(theFilename){
            var fileURL = $("#imageRow").find("input").val();
            
            if(fileURL!=""){
                var win = function (r) {
                    
                    console.log("Code = " + r.responseCode);
                    console.log("Response = " + r.response);
                    console.log("Sent = " + r.bytesSent);
                    console.log("Filename = " + theFilename);
                    if(isJson(r.response)){
                        var json = JSON.parse(r.response);
                        if(json.status!="Uploaded"){
                            logThis("add", "uploadImage()", json.status, "", "Win Function Image Upload");
                            return false;
                        }
                        
                    }
                    
                    
                    submitTheForm(theFilename); // SUBMIT THE FORM WHEN THE UPLOAD IS READY
                    
                }

                var fail = function (error) {
                    //alert("An error has occurred: Code = " + error.code);
                    console.log("upload error source " + error.source);
                    console.log("upload error target " + error.target);
                    logThis("add", "uploadImage()", error.code, error.target, theFilename);
                }
                
                try{
                    var options = new FileUploadOptions();
                    options.fileKey = "file";
                    options.fileName = fileURL.substr(fileURL.lastIndexOf('/') + 1);
                    options.mimeType = "image/jpeg";

                    var params = {};
                    params.theName = theFilename;

                    options.params = params;
                    options.chunkedMode = false;

                    var ft = new FileTransfer();
                    ft.upload(fileURL, encodeURI("http://"+base+"."+url+"/apicall/safe/_imageSave"), win, fail, options);
                }catch(err){
                    logThis("add", "uploadImage()", "", err, "");
                }
                return theFilename;
            }else{
                return null;
            }
        }    
            
        
        
        
        
        //CHECK GENDER
        
        function checkGender(){
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_userGender',
                data: {'fingerprint':fingerprint},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    if(json.reply==true)$("#genderRow").remove();
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("add", "userGender", xhr.status, thrownError, xhr.responseText);
                  }
            });
        }
        
        
