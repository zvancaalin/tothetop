function genId(wide) {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 1; i <= wide; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


function isJson(str) {
    try {
        JSON.parse(str);
    }catch(err){
        return false;
    }
    return true;
}

function componentFromStr(numStr, percent) {
    var num = Math.max(0, parseInt(numStr, 10));
    return percent ?
        Math.floor(255 * Math.min(100, num) / 100) : Math.min(255, num);
}

function rgbToHex(rgb) {
    var rgbRegex = /^rgb\(\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*,\s*(-?\d+)(%?)\s*\)$/;
    var result, r, g, b, hex = "";
    if ( (result = rgbRegex.exec(rgb)) ) {
        r = componentFromStr(result[1], result[2]);
        g = componentFromStr(result[3], result[4]);
        b = componentFromStr(result[5], result[6]); 
        hex = "#" + (0x1000000 + (r << 16) + (g << 8) + b).toString(16).slice(1);
    }
    return hex;
}


function logThis(page, action, status, error, text){
    alert("Something happened! Try again later!");
    var dataSend = {
        "page": page,
        "action": action,
        "status": status,
        "error": error,
        "text": text
    };
   // alert(action + " / "+status+" / "+error+" / "+text);
    $.ajax({
        url: 'http://'+base+"."+url+'/apicall/safe/_logThis', 
        type: 'POST',
        data: dataSend,
        success: function(dataR) {
           // alert(dataR);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            //alert("status: "+xhr.status);
            //alert(thrownError);
          }
    });
   
    if(status=="410")location.reload();
    $("#loader").fadeOut(400); 
}

function freeze(showw){
showw = (typeof showw !== 'undefined') ?  showw : true;
    if(showw == true){
        $("#freeze").removeClass("hide");
    }else{
        $("#freeze").addClass("hide");
    }
}



function notifyThis(type, text, hider){
hider = (typeof hider !== 'undefined') ?  hider : true;
     var notifier = $("#notifier");
                
    notifier.find("span#alertText").html(text+"!"); // FILL THE CONTENT

    notifier.addClass("alert-"+type); // SET THE NOTIFICATION TYPE

    notifier.removeClass("hide"); // SHOW THE NOTIFICATION
    if(hider)
    notifier.delay(3000).slideUp(500, function(){ // HIDE THE NOTIFICATION
        notifier.slideUp(500, function(){
            notifier.addClass("hide");
        });
    }); 
}


        function setOnBoarding(){
            var data ={'fingerprint':fingerprint};
            //alert("called");
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/set/_onBoarding',
                data: {
                    'fingerprint':fingerprint,
                }, 
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function() {
                    var storage = window.localStorage;
                    storage.setItem("onboarding", 1); // Pass a key name and its value to add or update that key. 
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "setOnBoarding", xhr.status, thrownError, xhr.responseText);
                  }
            });
        }




        function prepareWardrobe(){ //PREPARING THE WARDROBE
            var mainRow = $(".wardroberow"); 
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:1},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    //alert(dataR);
                    var json = JSON.parse(dataR);
                    // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#head.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#head.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:2},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#top.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#top.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:3},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW

                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#bottom.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#bottom.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                    //mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:4},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#shoes.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#shoes.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                   // mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_selectClothes',
                data: {'fingerprint':fingerprint, type:5},
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    var json = JSON.parse(dataR);
                    //var mainRow = $(".wardroberow"); // GET THE MAIN ROW
                    var rowClone = ""; // ROW CLONE DEFAULT
                    var itemClone = "";  // ITEM CLONE DEFAULT
                    var firstChild = "";
                    for(var i=1; i<=json.length; i++){ // PARSING THE CLOTHES ARRAY
                        if(i%4==1)rowClone = mainRow.clone(); // IF IT's THE FIRST ITEM IN ROW CLONE THE ROW
                        firstChild = rowClone.find(".wardrobeChild:nth-child(1)"); // GET THE FIRST ITEM IN ROW
                        itemClone = firstChild.clone(); //CLONE THE FIRST ITEM IN ROW
                        
                        if(typeof json[i-1].picture !== "undefined" && json[i-1].picture!="")
                            itemClone.find(".wardrobe_item").css("background", "#eaeaea url(http://hosting.touchads.ml/min-"+json[i-1].picture+") 100% center/cover").css("animation-name", "none"); // IF THERE IS ANY PICTURE FOR THE CLOTHE PLACE IT AS A BACKGROUND
                        itemClone.find(".wardrobe_item").css("animation-name", "none");
                        itemClone.attr("data-id", json[i-1].id);
                        itemClone.appendTo(rowClone); //APPEND THE NEW ITEM TO THE CLONED ROW
                        
                        if(i%4==0){firstChild.remove(); rowClone.appendTo("#accesories.tab-pane");} // IF IT's THE LAST ITEM IN ROW DELETE THE PUPPET ITEM AND APPEND THE ROW TO THE WRAPPER
                    }
                    if(json.length%4!=0){firstChild.remove(); rowClone.appendTo("#accesories.tab-pane");} // IF THE NUMBER OF CLOTHES IS BIGGER THAN A ROW APPEND THE LAST ITEMS TO THE WRAPPER
                   // mainRow.remove(); // REMOVE THE PUPPET ROW
                    //alert($("#dulap").html());
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    logThis("index", "prepareWardrobe", xhr.status, thrownError, xhr.responseText);
                  }
            });
            mainRow.remove();
        }
           

        
        // GENERATE FORM INPUTS
        
        function generateInput(formWrap){
            formWrap.find("input").remove();
            $("#tagSpace .bigSelect.active").each(function(){
                var inputHTML="<input type='checkbox' name='tags[]' style='display:none;' value='"+$(this).attr("data-id")+"' checked>";
                formWrap.append(inputHTML);
                
               // alert($("#topPanelContent").html());
            });
            $("#colorSpace .colorSelect.active").each(function(){
                var inputHTML="<input type='checkbox' name='colors[]' style='display:none;' value='"+$(this).attr("data-id")+"' checked>";
                
                formWrap.append(inputHTML);
                
               // alert($("#topPanelContent").html());
            });
        }
        
        
        
        
        
        
        
        
        // GENERATE FORM DATA
        
        
        function generateData(){
            try{
            
                /// CREATE TAGS BUTTONS FROM DB


                var tagTemplate = $("#tagTemplate");


                $.ajax({
                    url: 'http://'+base+"."+url+'/apicall/safe/_tagsList', 
                    type: 'POST',
                    success: function(dataR) {

                        var json = JSON.parse(dataR);

                        for(var i=0; i<json.length; i++){

                            var currentTemplate = tagTemplate.clone();

                            if(i==0) {   // Check the first checkbox
                                currentTemplate.addClass("active");
                            }

                            currentTemplate.attr("data-id", json[i].id);   // Set The Value for each checkbox
                            currentTemplate.removeAttr("id");    // Remove ID attribute

                             // Get the input html ONLY

                            currentTemplate.html(json[i].name); // Change the Text for each checkbox
                            //alert(currentTemplate.html());
                            currentTemplate.appendTo("#tagSpace"); // Move the checkbox in right place
                            currentTemplate.removeClass("hide"); // Show the Checkbox



                        } 
                        tagTemplate.remove();


                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        //alert("status: "+xhr.status);
                        //alert(thrownError);
                        logThis("index", "generateData:Tags", xhr.status, thrownError, xhr.responseText);
                      }
                });
                
                
                /// CREATE COLORS BUTTONS FROM DB


                var colorTemplate = $("#colorTemplate");
                var toggle = $("#showRestColorsTemplate");

                $.ajax({
                    url: 'http://'+base+"."+url+'/apicall/safe/_colorsList', 
                    type: 'POST',
                    success: function(dataR) {

                        var json = JSON.parse(dataR);

                        for(var i=0; i<json.length; i++){

                            var currentTemplate = colorTemplate.clone();

                            if(i==0) {   // Check the first checkbox
                                currentTemplate.addClass("active");
                            }
                            
                            currentTemplate.attr("data-id", json[i].id); // SET THE ID

                            currentTemplate.removeAttr("id");    // Remove ID attribute

                            currentTemplate.find(".colorDot").css("background-color", json[i].color);
                            //alert(currentTemplate.html());
                            if(i<3){
                                currentTemplate.appendTo("#colorSpace"); // Move the checkbox   in right place
                               
                            }else
                                currentTemplate.appendTo("#colorSpace.rest");    
                            
                            currentTemplate.removeClass("hide"); // Show the ColorDot
 


                        } 
                        colorTemplate.remove();
                        toggle.clone().removeClass("hide").appendTo("#colorSpace");
                        toggle.remove();

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        logThis("index", "generateData:Colors", xhr.status, thrownError, xhr.responseText);
                      }
                });
            
            
            
            }catch(err){
                logThis("index", "generateData:ALL", "", err, "");
            }
            
       }
        
        
       


        
        function urlManagement(){ // URL MANAGEMENT FUNCTION
            
            var pathArray = location.href.split( '#' ); //GET THE URL AND SPLIT
            
            var secondLevelLocation = pathArray[1]; // GET THE STRING AFTER THE # SIGN
            
            var cellButtonGroup = $('ul#tablist');
            var cellButtons = cellButtonGroup.find('li');
            //alert(secondLevelLocation);
            if(typeof secondLevelLocation!=="undefined"){ // IF THERE IS ANY LINK SHOW THE TAB
                //alert(secondLevelLocation);
                cellButtons.filter('.active').removeClass('active');
                $('#tablist li').eq(secondLevelLocation).addClass("active");
                $('.carousel').flickity( 'select', secondLevelLocation); 
            }
            
        }
         
        function notifierManagement(){
            var pathArray = location.href.split( '#' ); //GET THE URL AND SPLIT
            
            var thirdLevelLocation = pathArray[2]; // GET THE STRING AFTER THE # SIGN
            //alert(secondLevelLocation);
            
            if(typeof thirdLevelLocation!=="undefined"){ // IF THERE IS ANY NOTIFICATION
                var splitAgain = thirdLevelLocation.split("?"); // SPLIT THE TYPE AND THE TEXT
                
                var type = splitAgain[0];
                
                var text = splitAgain[1];
                
                var notifier = $("#notifier");
                
                notifier.find("span#alertText").html(text+"!"); // FILL THE CONTENT
                
                notifier.addClass("alert-"+type); // SET THE NOTIFICATION TYPE
                
                notifier.removeClass("hide"); // SHOW THE NOTIFICATION
                
                notifier.delay(3000).slideUp(500, function(){ // HIDE THE NOTIFICATION
                    notifier.slideUp(500, function(){
                        notifier.addClass("hide");
                    });
                });   
            }
        }
        
        

        function getNoClothes(){
            var data ={'fingerprint':fingerprint};

            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/get/_noOfClothes',
                data: {
                    'fingerprint':fingerprint,
                }, 
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400); 
                            return false;
                        }
                        noClothes = json.nr;
                        if(noClothes>0 && json.a){
                            $("#home_title").addClass("hide");
                            $("#randomize").removeClass("disabled");
                        }else $("#randomize").removeAttr("id");
                        $("#loader").fadeOut(400);
                    } 
                    //alert(dataR);
                    
                     
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
        }
        

        $("body").on("touchend", "#addnew", function(){ // VERY QUICK EVENT TRIGGER FOR ADD PAGE REDIRECT
            location.href="add_page/add.html";
        });
        
        
        
        $("body").on("touchend", "#closeWardrobeItem", function(e){ // VERY QUICK EVENT TRIGGER FOR MODAL CLOSE
            e.preventDefault();
            $(".modal").modal("hide");
            e.stopPropagation();
        });
        
        
        
        $('body').on("touchend", "#tablist a", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
           // $(this).tab("show");
            location.href=$(this).attr("href"); 
        });
        
        $('body').on("touchend", "#wtablist a", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
            $(this).tab("show");
            //location.href=$(this).attr("href"); 
        });
        
        $('body').on("touchend", "#showRestColorsTemplate", function(){ //VERY QUICK EVENT TRIGGER FOR TAB CHANGE
            //alert($("#colorSpace.rest").html());
            if($("#colorSpace.rest").is(":hidden")){
                $(".headerScrollable").height($("#topPanelContent").height());
            }
            $("#colorSpace.rest").slideToggle(200, function(){
                if($("#colorSpace.rest").is(":hidden")){
                    $("#showRestColorsTemplate").find("span").removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down").css("top", "2px");
                }else{
                    $("#showRestColorsTemplate").find("span").removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up").css("top", "0px");
                }
            });
        });
        
        
        
        
        var touchmoved3; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#tagSpace').on('touchend', "label.btn", function(e){
            if(touchmoved3 != true){
                e.preventDefault();
                $(this).button("toggle");
                //e.stopPropagation(); 
                
                
                //alert($(this).wrap('<p/>').parent().html());
            }
        }).on('touchmove', function(e){
            touchmoved3 = true;
        }).on('touchstart', function(){
            touchmoved3 = false;
        });
        
        
         var touchmoved4; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#colorSpace, #colorSpace.rest').on('touchend', "label.colorSelect", function(e){
            if(touchmoved4 != true){
                e.preventDefault();
                $(this).button("toggle");
                //e.stopPropagation(); 
            }
        }).on('touchmove', function(e){
            touchmoved4 = true;
        }).on('touchstart', function(){
            touchmoved4 = false;
        });
        
        
        
        var touchmoved5; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('header.header').on('touchend', "#hidePanel", function(e){
            if(touchmoved5 != true){
                $(".headerScrollable").slideToggle(300, function(){
                    if($(".headerScrollable").is(":hidden"))
                        $("#hidePanel").text("Show");
                    else
                        $("#hidePanel").text("Hide");
                });
            }
        }).on('touchmove', function(e){
            touchmoved5 = true;
        }).on('touchstart', function(){
            touchmoved5 = false;
        });

var touchmoved7; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('.onboarding').on('touchend', ".skipButton", function(e){
            if(touchmoved7 != true){
                $(".onboarding").remove();
            }
        }).on('touchmove', function(e){
            touchmoved7 = true;
        }).on('touchstart', function(){
            touchmoved7 = false;
        });
        
        
        
        $("header").on("touchend", "#showMe", function(e){
            e.preventDefault();
            
            $("#loader").show();  
            
            generateInput($("#topPanelContent"));
            
            $("#umerasCustom").removeClass("hide");//SHOW THE LOADING DOTS
            
            dataSend={ // SERIALIZE THE FORM AND CREATE THE DATA ARRAY
                  "form":  $("#topPanelContent").serializeArray(),
                  "fingerprint": fingerprint
            };
            //alert(fingerprint);
            $.ajax({
                url: 'http://'+base+"."+url+'/apicall/get/_customRandomizer',
                data: dataSend,
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST', 
                success: function(dataR) { 
                   //alert(dataR);
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            $("#loader").fadeOut(400); 
                            alert(json.error); 
                            return false;
                        }
                    }  
                       
                    //alert(dataR);
                    $("#umerasCustom").html(dataR);
                    
                    $("#umerasCustom .dresspot").css("animation-name", "none"); 
                    
                    $(".headerScrollable").slideUp(300, function(){
                            $("#hidePanel").text("Show");
                    });
                    
                    $("#loader").fadeOut(200); // HIDE THE LOADER
                    //$("#randomize").removeAttr("disabled"); 
                     
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert("status: "+xhr.responseText);
                    //alert(thrownError); 
                    $("#loader").fadeOut(400); 
                    logThis("index", "customRandomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
        });  
         
        //// RANDOMIZE CLICK
        $("body").on("touchend", "#randomize", function(){
            //$(this).addClass("disabled");
            $("#loader").show();
            $(this).css("color", "#fff");
            $('.carousel').flickity( 'select', 0 );
            
            $("#splash").addClass("hide");//HIDE THE LOGO
            
            $("#umeras").removeClass("hide");//SHOW THE LOADING DOTS
            
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/get/_Randomizer',
                data: {
                    'fingerprint':fingerprint,
                },
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400);  
                            return false;
                        }
                    } 
                    //alert(dataR);
                    $("#umeras").html(dataR);
                    $(".dresspot").css("animation-name", "none"); 
                    $("#loader").fadeOut(200); // HIDE THE LOADER
                   // $("#randomize").removeAttr("disabled");
                    //alert(dataR);
                    // Decode and show the returned data nicely.
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
       
        });

        

    ////// DELETE EVENT
 $("body").on("touchend", ".deleteClothes", function(){
            //$(this).addClass("disabled");
            $("#loader").show();
            
            var clothes_id = $(this).attr("data-id");
            
            $.ajax({ 
                url: 'http://'+base+"."+url+'/apicall/set/_deleteClothes',
                data: {
                    'fingerprint':fingerprint,
                    'clothes_id':clothes_id
                },
                beforeSend: function(request){
                    request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                    request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                },
                type: 'POST',
                success: function(dataR) {
                    if(isJson(dataR)){
                        var json = JSON.parse(dataR);
                        if(typeof json.error!=="undefined"){
                            alert(json.error); 
                            $("#loader").fadeOut(400); 
                            return false;
                        } 
                        if(typeof json.rez!=="undefined"){
                            if(json.rez=="success"){
                                
                                $("#loader").fadeOut(400);
                                location.href="index.html#wardrobe#success?You have deleted the product successfully";
                                location.reload();
                            }else{
                                $("#loader").fadeOut(400);
                                logThis("add", "deleteClothes", "rez", "", "rez != success");
                                //alert("Something happened! Try again later!");
                            }
                        }else{
                            $("#loader").fadeOut(400);
                            logThis("add", "deleteClothes", "rez", "", "rez is missing");
                        }
                    }else{
                        logThis("add", "deleteClothes", "", "", "Json response not properly formatted!");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    //alert("status: "+xhr.status);
                    //alert(thrownError);
                    $("#loader").fadeOut(400); 
                    logThis("index", "Randomizer", xhr.status, thrownError, xhr.responseText);
                  }
            });
       
        });
        
        
        
        ///////// MODAL CONTENT GETTERS
        




var touchmoved; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#dulap').on('touchend', " .wardrobeChild", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            //alert("merge");
            if(touchmoved != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!== "undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            //alert("status: "+xhr.status);
                            //alert(thrownError);
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved = false; // IT MEANS IT DOESN'T MOVING YET
        });
       
        
        var touchmoved2; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#umeras').on('touchend', ".dresspot", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            //alert("merge");
            if(touchmoved2 != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!=="undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent2").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal2').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent:home", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved2 = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved2 = false; // IT MEANS IT DOESN'T MOVING YET
        });
        



var touchmoved6; // CHECK IF THE FINGER WAS MOVED ON THE SCREEN TO PREVENT CLICKING WHEN SCROLLING
        $('#etc').on('touchend', ".dresspot", function(e){ // FUNCTION WHEN YOU REMOVE THE FINGER FROM THE SCREEN
            
            if(touchmoved6 != true){ //IF IT WASN"T A MOVE OF YOUR FINGER ON SCREEN AND JUST A TAP , MAKE THE MAGIC
                    $("#loader").show();
                    var clothes_id=$(this).attr("data-id");
                    //alert(clothes_id);
                    $.ajax({
                        url: 'http://'+base+"."+url+'/apicall/get/_clothesModalContent',
                        data: {
                            'fingerprint':fingerprint,
                            'clothes_id':clothes_id
                        },
                        beforeSend: function(request){
                            request.setRequestHeader('Authorization', 'Bearer ' + store.JWT);
                            request.setRequestHeader('FG', 'Foxing ' + fingerprint);
                        },
                        type: 'POST',
                        success: function(dataR) {
                            if(isJson(dataR)){
                                var json = JSON.parse(dataR);
                                if(typeof json.error!=="undefined"){
                                    alert(json.error); 
                                    return false;
                                }
                            }
                            //alert(dataR);
                            
                            $("#modalRemoteContent3").html(dataR); // GET THE MODAL CONTENT 
                            $('#myModal3').modal('show');// SHOW THE MODAL
                            $("#loader").fadeOut(400); // HIDE THE LOADER
                            //alert(dataR);
                            // Decode and show the returned data nicely.
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            $("#loader").fadeOut(400);
                            logThis("index", "clothesModalContent:home", xhr.status, thrownError, xhr.responseText);
                          }
                    });
       
                
                }
        }).on('touchmove', function(e){ // IF THE FINGER IS MOVING ON SCREEN
            touchmoved6 = true; // WE HAVE TO MENTION THAT
        }).on('touchstart', function(){ // IF THE FINGER TOUCHES THE SCREEN
            touchmoved6 = false; // IT MEANS IT DOESN'T MOVING YET
        });
        
        
        
        
        ///////// END OF MODAL CONTENT GETTERS
        







var gnStartTime = 0;
var gbMove = false;
var gbStillTouching = false;
var show = 0;
function checkTapHold(nID) {
  if ((!gbMove) && (gbStillTouching) && (gnStartTime == nID)) {
    gnStartTime = 0;
    gbMove = false; 
    //alert('tap hold event'); 
      if(!show){
          show=1;
          $(".modal-body").css("background", "rgba(0,0,0,0.2)");
      }
  }
}

window.addEventListener('touchstart',function(event) {
  gbMove = false;
  gbStillTouching = true;
  gnStartTime = Number(new Date());
  setTimeout('checkTapHold(' + gnStartTime + ');',300);
},false);

window.addEventListener('touchmove',function(event) {
  gbMove = true;
},false);

window.addEventListener('touchend',function(event) {
  gbStillTouching = false;
    $(".modal-body").css("background", "rgba(0,0,0,0.7)");
    show=0;
},false);


$("button.disabled").on("touchstart touchend click", function(e){
    $(this).off();
    $(this).unbind();//alert("disabled");
    e.preventDefault();
    return false;
});
